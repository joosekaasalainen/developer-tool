//
//	Insolo Developer Tool (Prototype)
//
//////////////////////////////////////////////////////////////

var ajaxURL = "/developertool/devtool/flushcache";
var toggleSpeed = 0.2;

//////////////////////////////////////////////////////////////

var open = false;

document.observe('dom:loaded', function() {

	// Panel right position
	var hidepos = $('dt_panel').getStyle('right');
	
	// Enable/disable flush button
	$$('.action').each(function(element) {
		element.observe("click", function() {
			if ( $$('.action:checked').length == 0 ) { dtDisableButton(); }
			else { dtEnableButton(); }
		});
	});
	
	// Open/close developer tool panel
	$('dt_openclose').observe("click", function() {
		if (open) { $('dt_panel').morph('right: '+hidepos+'', { duration: toggleSpeed }); open = false; } 
		else { $('dt_panel').morph('right: 0', { duration: toggleSpeed }); open = true; }
	});

});



function dtEnableButton() {
	$('dt_flush_button').removeAttribute('disabled');
}
function dtDisableButton() {
	$('dt_flush_button').writeAttribute('disabled','disabled');
}



function DevtoolFlush() {
	
	var data = new Array();
	$$('.action').each(function(e){
		if (e.checked==true) {
			data.push(e.value);
		}
	});
	
	new Ajax.Request(ajaxURL, {
		method: 'post',
		parameters: {'action[]': data},
		onCreate: function() {
			dtDisableButton();
			$('dt_status').update();
			$('dt_status').removeClassName("dt_ready");
			$('dt_status').setStyle({display: 'block'});
		},
		onSuccess: function(response) {
		if (response.status == 200) {
			$('dt_status').addClassName("dt_ready");
			$('dt_status').update(response.responseText);
			dtEnableButton();
		}
	  }
	});

}