dt = jQuery.noConflict();
var open = false;
var toggleSpeed = 400;

dt(document).ready(function(){

	// Show jQuery version
	//var version = dt.fn.jquery;
	//alert("DT: "+version);

	var hidepos = (dt('#dt_panel').css("right"));

	dt('.action').change(function() {
		if (dt('.action:checked').length) { dtEnableButton(); }
		else { dtDisableButton(); }
	});
	
	dt('#dt_openclose').click(function() { 
		if (open) { dt('#dt_panel').animate({right:""+hidepos+""},toggleSpeed); open = false; } else { dt('#dt_panel').animate({right:"0"},toggleSpeed); open = true; }
	});
	
});

function dtEnableButton() {
	dt("#dt_flush_button").removeAttr('disabled');
}
function dtDisableButton() {
	dt("#dt_flush_button").attr('disabled','disabled');
}

function DevtoolFlush() {
	
	var data = dt('input:checkbox:checked').map(function(){
		return this.value;
	}).get();
	
	dt('#dt_status').ajaxStart( function() { dt('#dt_status').empty(); dt(this).removeClass("dt_ready"); dtDisableButton(); dt(this).show(); } );

	dt.ajax({ url: '/developertool/devtool/flushcache',
			 data: {action: data},
			 type: 'post',
			 success: function(output) {
				dt('#dt_status').addClass("dt_ready");
				dt("#dt_status").html(output);
				
				dtEnableButton()
				//resetStatus();
			}
	});

}

function resetStatus() {
	dt('#dt_status').fadeOut("fast", function() { dt(this).removeClass("dt_ready"); });
}