<?php
	class Insolo_Developertool_Block_Dt extends Mage_Catalog_Block_Product
	{    
		function getSettings() {
			return Mage::getStoreConfig('dt-options/dt-settings');
		}
		
		 protected function _prepareLayout()
		{
			$s = $this->getSettings();
			$enabled = $s['dt-enabled'];
			if ($enabled) {
				$this->getLayout()->getBlock('head')->addJs("insolo/developertool/dt-prototype.js","name=\"dtjs\"");
				return parent::_prepareLayout();
			}
		}
	}
?>