<?php

# https://github.com/akira28/magento-utils/blob/master/shell/cleanCache.php
# by Andrea De Pirro

class Insolo_Developertool_DevtoolController extends Mage_Core_Controller_Front_Action
{
    public function flushcacheAction() {
		
		if ($this->getRequest()->isPost()){
		
			$action = $this->getRequest()->getPost('action');

			foreach($action as $flush) {

			$types = Mage::app()->getCacheInstance()->getTypes();
				
				switch($flush) {
				
					// Flush Magento Cache
				
					case "flushCache":

						try {
							echo "Cleaning data cache:<br>" . PHP_EOL;
							flush();
							$types = Mage::app()->getCacheInstance()->getTypes();
							foreach ($types as $type => $data) {
								echo "Removing $type ... ";
								echo Mage::app()->getCacheInstance()->clean($data["tags"]) ? "[OK]" : "[ERROR]";
								echo "<br>";
								echo PHP_EOL;
							}
							echo PHP_EOL;
						}
						catch (Exception $e) {
							die("[ERROR:" . $e->getMessage() . "]" . PHP_EOL);
						}
						
						break;
						
					
					// Flush Cache Storage

					case "flushStorage":
					
						try {
							require_once('app/Mage.php');
							Mage::app()->getCacheInstance()->flush();
							echo "Cache storage flushed!<br>";
						}
						catch(Exception $e)
						{
							echo 'Message: ' .$e->getMessage();
						}

						break;

						
					// Flush Catalog Images Cache
						
					case "flushImages":

						try {
							echo "Cleaning image cache... ";
							flush();
							echo Mage::getModel('catalog/product_image')->clearCache();
							echo "[OK]" . PHP_EOL . PHP_EOL;
						}
						catch (Exception $e) {
							die("[ERROR:" . $e->getMessage() . "]" . PHP_EOL);
						}

						break;

						
					// Flush Javascript/CSS Cache
						
					case "flushJSCSS":

						try {
							echo "Cleaning merged JS/CSS... ";
							flush();
							Mage::getModel('core/design_package')->cleanMergedJsCss();
							Mage::dispatchEvent('clean_media_cache_after');
							echo "[OK]" . PHP_EOL . PHP_EOL;
						}
						catch (Exception $e) {
							die("[ERROR:" . $e->getMessage() . "]" . PHP_EOL);
						}
						
						break;

				}
				
			}
		}
    }
}

?>