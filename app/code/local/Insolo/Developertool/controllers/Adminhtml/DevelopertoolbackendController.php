<?php
class Insolo_Developertool_Adminhtml_DevelopertoolbackendController extends Mage_Adminhtml_Controller_Action
{
	public function indexAction()
    {
       $this->loadLayout();
	   $this->_title($this->__("Developer Tool"));
	   $this->renderLayout();
    }
}